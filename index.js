const express = require ('express');
const mongoose = require('mongoose');
const dotenv = require ('dotenv');
dotenv.config();
const app = express();
const PORT = 3006;


// Middlewares - Can get in express js documentation
app.use(express.json())
app.use(express.urlencoded({extended:true}))


// Mongoose connection
    // mongoose.connect(<connection string>, {from})

mongoose.connect (process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true} )

// DB connection notification
    // use connection property of mongoose

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', () => {
        console.log(`Connected to Database`)
      // we're connected!
    });

    //1.
    //Create a schema for users
        //schema determines the structure of the document written in our database
        //
    const userSchema = new mongoose.Schema({
        username: {
            type: String,
            required: [true, `username is required`]
        },
        password: {
            type: String,
            required: [true, `password is required`]
        }
      });
    // create a model out of the schema
        // mongoose.model(<name of the model>,<schema model came from>)
        
        // model is a programming interface that enables us to query and manipulate database using its methods
        const Users = mongoose.model('Users', userSchema);

        app.post('/signup', (req,res)=> {
            console.log(req.body)
            Users.findOne({username: req.body.username, password: req.body.password})
            .then ((result,err) => {
                console.log(result) //result = a document

                if(result != null && result.name == req.body.name){
                    return res.send(`Username already exists!`)
                } else {
                   let newUsers = new Users({
                       username: req.body.username,
                       password: req.body.password
                   })

                   //saves a document in our database 
                   //a mongoose method save()
                   newUsers.save().then((result, err)=> {
                        // console.log(result)

                        if(result){
                            return res.send(result)
                        }else {
                            return res.status(500).send(err)
                        }
                   })

                } 
            })
            
            })

app.listen(PORT, ()=> console.log(`Server connected at port ${PORT}`))


